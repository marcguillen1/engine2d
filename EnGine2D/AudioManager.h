#pragma once
#include "includes.h"
#include "audio.h"

class AudioManager
{
private:
	static AudioManager* instance;
	AudioManager();
	std::map<typeOfSound, Audio*> mSoundMapAudio;
public:
	~AudioManager();
	static AudioManager* getInstance();
	void addAudio(typeOfSound audiotype);
	Audio* getAudio(typeOfSound audiotype);

};

