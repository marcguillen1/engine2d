#include "Text.h"
#include "singletons.h"



Text::Text() : Widgets()
{
	currentRect = new C_Rectangle();
}

Text::~Text()
{

}
void Text::drawText(std::string text, int x, int y)
{
	int len = text.length();

	int srcX = 0, srcY = 0;
	int srcWidth = 0, srcHeight = 0;

	for (int i = 0; i < len; i++)
	{
		char character = (char)text[i];

		if (character == ' ') {
			x += srcWidth;
			continue;
		}

		srcX = 0;
		srcY = 0;
		srcWidth = 30;
		srcHeight = 35;

		switch (character)
		{
			//letters
		case 'a':
			srcX = 0;
			srcY = srcHeight;
			currentRect->x = 0;
			currentRect->y = srcHeight;
			break;
		case 'b':
			srcX = srcWidth;
			srcY = srcHeight;
			currentRect->x = srcWidth;
			currentRect->y = srcHeight;
			break;
		case 'c':
			srcX = srcWidth * 2;
			srcY = srcHeight;
			currentRect->x = srcWidth * 2;
			currentRect->y = srcHeight;
			break;
		case 'd':
			srcX = srcWidth * 3;
			srcY = srcHeight;
			currentRect->x = srcWidth * 3;
			currentRect->y = srcHeight;
			break;
		case 'e':
			srcX = srcWidth * 4;
			srcY = srcHeight;
			currentRect->x = srcWidth * 4;
			currentRect->y = srcHeight;
			break;
		case 'f':
			srcX = srcWidth * 5;
			srcY = srcHeight;
			currentRect->x = srcWidth * 5;
			currentRect->y = srcHeight;
			break;
		case 'g':
			srcX = srcWidth * 6;
			srcY = srcHeight;
			currentRect->x = srcWidth * 6;
			currentRect->y = srcHeight;
			break;
		case 'h':
			srcX = srcWidth * 7;
			srcY = srcHeight;
			currentRect->x = srcWidth * 7;
			currentRect->y = srcHeight;
			break;
		case 'i':
			srcX = srcWidth * 8;
			srcY = srcHeight;
			currentRect->x = srcWidth * 8;
			currentRect->y = srcHeight;
			break;
		case 'j':
			srcX = srcWidth * 9;
			srcY = srcHeight;
			currentRect->x = srcWidth * 9;
			currentRect->y = srcHeight;
			break;
		case 'k':
			srcX = srcWidth * 10;
			srcY = srcHeight;
			currentRect->x = srcWidth * 10;
			currentRect->y = srcHeight;
			break;
		case 'l':
			srcX = srcWidth * 11;
			srcY = srcHeight;
			currentRect->x = srcWidth * 11;
			currentRect->y = srcHeight;
			break;
		case 'm':
			srcX = srcWidth * 12;
			srcY = srcHeight;
			currentRect->x = srcWidth * 12;
			currentRect->y = srcHeight;
			break;
		case 'n':
			srcX = srcWidth * 13;
			srcY = srcHeight;
			currentRect->x = srcWidth * 13;
			currentRect->y = srcHeight;
			break;
		case 'o':
			srcX = 0;
			srcY = srcHeight * 2;
			currentRect->x = 0;
			currentRect->y = srcHeight * 2;
			break;
		case 'p':
			srcX = srcWidth;
			srcY = srcHeight * 2;
			currentRect->x = srcWidth;
			currentRect->y = srcHeight * 2;
			break;
		case 'q':
			srcX = srcWidth * 2;
			srcY = srcHeight * 2;
			currentRect->x = srcWidth * 2;
			currentRect->y = srcHeight * 2;
			break;
		case 'r':
			srcX = srcWidth * 3;
			srcY = srcHeight * 2;
			currentRect->x = srcWidth * 3;
			currentRect->y = srcHeight * 2;
			break;
		case 's':
			srcX = srcWidth * 4;
			srcY = srcHeight * 2;
			currentRect->x = srcWidth * 4;
			currentRect->y = srcHeight * 2;
			break;
		case 't':
			srcX = srcWidth * 5;
			srcY = srcHeight * 2;
			currentRect->x = srcWidth * 5;
			currentRect->y = srcHeight * 2;
			break;
		case 'u':
			srcX = srcWidth * 6;
			srcY = srcHeight * 2;
			currentRect->x = srcWidth * 6;
			currentRect->y = srcHeight * 2;
			break;
		case 'v':
			srcX = srcWidth * 7;
			srcY = srcHeight * 2;
			currentRect->x = srcWidth * 7;
			currentRect->y = srcHeight * 2;
			break;
		case 'w':
			srcX = srcWidth * 8;
			srcY = srcHeight * 2;
			currentRect->x = srcWidth * 8;
			currentRect->y = srcHeight * 2;
			break;
		case 'x':
			srcX = srcWidth * 9;
			srcY = srcHeight * 2;
			currentRect->x = srcWidth * 9;
			currentRect->y = srcHeight * 2;
			break;
		case 'y':
			srcX = srcWidth * 10;
			srcY = srcHeight * 2;
			currentRect->x = srcWidth * 10;
			currentRect->y = srcHeight * 2;
			break;
		case 'z':
			srcX = srcWidth * 11;
			srcY = srcHeight * 2;
			currentRect->x = srcWidth * 11;
			currentRect->y = srcHeight * 2;
			break;


			//nums & signs			
		case '0':
			srcX = 0;
			srcY = 0;
			break;
		case '1':
			srcX = srcWidth;
			srcY = 0;
			break;
		case '2':
			srcX = srcWidth * 2;
			srcY = 0;
			break;
		case '3':
			srcX = srcWidth * 3;
			srcY = 0;
			break;
		case '4':
			srcX = srcWidth * 4;
			srcY = 0;
			break;
		case '5':
			srcX = srcWidth * 5;
			srcY = 0;
			break;
		case '6':
			srcX = srcWidth * 6;
			srcY = 0;
			break;
		case '7':
			srcX = srcWidth * 7;
			srcY = 0;
			break;
		case '8':
			srcX = srcWidth * 8;
			srcY = 0;
			break;
		case '9':
			srcX = srcWidth * 9;
			srcY = 0;
			break;
		case ':':
			srcX = srcWidth * 10;
			srcY = 0;
			break;
		case '?':
			srcX = srcWidth * 11;
			srcY = 0;
			break;
		case '!':
			srcX = srcWidth * 12;
			srcY = 0;
			break;
		case '.':
			srcX = srcWidth * 13;
			srcY = 0;
			break;
		}

		widgtrect = currentRect;
		sVideoManager->renderGraphic(fileText, widgtrect, x, y);

		x += srcWidth;
	}
}