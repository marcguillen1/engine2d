#pragma once
#include "Camera.h"
#include "Entity.h"
#include "Character.h"
#include "Wall.h"
#include "Background.h"
#include "Text.h"

class World
{
private:
	static World* instance;
	float tickTime;
	Character* mainCharacter;
	C_Rectangle ground;
	std::vector<Background*> backgrounds;
public:
	int bgX, bgX2, bgY, bgY2;
	World();
	~World();
	void initWorld();
	void renderWorld();
	void updateWorld(float time);
	void addEntity(Entity* aEntity);
	void removeEntity(Entity* aEntity);
	void removeallEntities();
	void removeall();
	void removeBackgrounds();
	void setMainCamera();
	void initBackgrounds();
	void updateBackgrounds();
	void renderBackgrounds();
	void setPlayerEntity(Character* entity);
	Character* getPlayerEntity();
	Entity* getEntity(int id);
	std::vector<C_Rectangle> getEntitiesColByType(Entity::typeEntity type);
	std::vector<Wall*> getWalls();
	C_Rectangle getGround();
	static World* getInstance();
	static Camera* playerCamera;
	std::map<std::string, Entity*> entities;
	std::vector<Entity*> vEntities;
	int score;
	int oldscore;

	/*std::map<std::string, Ent*/
};
////
////wolrd
////std::string temp_id = tostring(entity->get_id());
//entity.insert(std::pair>std::string, Entity*(temp_id))