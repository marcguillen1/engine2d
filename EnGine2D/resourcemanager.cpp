#include "singletons.h"


ResourceManager* ResourceManager::pInstance = NULL;

ResourceManager* ResourceManager::getInstance()
{
	if (pInstance == NULL) {
		pInstance = new ResourceManager();
	}
	return pInstance;
}

ResourceManager::ResourceManager()
{

}

ResourceManager::~ResourceManager(){
	mGraphicsMapTexture.clear();
}

void ResourceManager::addGraphic(const char* file)
{
	//Se crea una superficie (surface) [SDL_Surface*] NULA
	SDL_Surface* surface = NULL;

	//Se carga el archivo en la surface -- IMG_Load(file)
	surface = IMG_Load(file);

	//Si surface =  NULL
	if (!surface)
	{
		std::cout << "ERROR SDL_SURFACE -- " << file << "line: ";
	}
	else{
		SDL_Texture* texture = NULL;
		texture = SDL_CreateTextureFromSurface(sVideoManager->getRenderer(), surface);
		if (!texture)
		{
			std::cout << SDL_GetError();
		}
		mGraphicsMapTexture.insert(std::pair<std::string, SDL_Texture*>(file, texture));
	}
}

SDL_Texture* ResourceManager::getGraphic(const char* file)
{
	std::map<std::string, SDL_Texture*>::iterator it;
	it = mGraphicsMapTexture.find(file);

	if (it == mGraphicsMapTexture.end())
	{
		std::cout << "Adding Texture" << file << "(first request)";
		addGraphic(file);
		it = mGraphicsMapTexture.find(file);
	}
	return it->second;
}

