#include "SceneGameOver.h"
#include "singletons.h"


SceneGameOver::SceneGameOver() : Scene()
{
	gotoMenu = false;
	homeButton = new Button();
	widgets.push_back(homeButton);
	homeButton->setIsCurrentButton(true);
	bg = new C_Rectangle();
}


SceneGameOver::~SceneGameOver()
{
}

void SceneGameOver::init(){
	homeButton->initWidget(SCREEN_WIDTH/2-(68), SCREEN_HEIGHT/2 + 20, 0, 0, 132, 32, RETURNBUTTON);
	sAudioManager->getAudio(GAMEOVER)->loadSound();
	sAudioManager->getAudio(GAMEOVER)->playSound();
	sAudioManager->getAudio(GAMEOVER)->setVolume(1);
	bg->x = 0;
	bg->y = 0;
	bg->w = 2400;
	bg->h = 1000;
}

void SceneGameOver::update(double seconds_elapsed)
{
	if (gotoMenu)
	{		
		sAudioManager->getAudio(GAMEOVER)->stopSound();
		sSceneManager->loadScene(0);
	}
}

void SceneGameOver::keyboardControl(SDL_Event kevent)
{

	mEvento = kevent;
	SDL_PumpEvents();

	if (mEvento.type == SDL_KEYDOWN) {
		switch (mEvento.key.keysym.scancode) {
		case SDL_SCANCODE_RETURN: if (homeButton->getIsCurrentButton()){
									  gotoMenu = true;
		}
			break;
		default:
			break;
		}
	}
}

void SceneGameOver::renderScene()
{
	sVideoManager->renderScaledGraphic("Assets/gameoverscreen.png", bg, 0, 0, 855, 480);
	for each (Widgets* w in widgets)
	{
		w->render();
	}

}