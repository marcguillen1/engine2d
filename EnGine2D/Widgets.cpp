#include "Widgets.h"
#include "singletons.h"


Widgets::Widgets()
{
/*	numID = numWidgets;
	numWidgets++*/;
	mytexture = NULL;
}

Widgets::~Widgets()
{

}
void Widgets::setName(std::string name)
{
	ID = name;
}

void Widgets::initWidget(float posX, float posY, float bX, float bY, float wh, float ht, Type type)
{
	beginX = posX;
	beginY = posY;
	width = wh;
	height = ht;
	widgtrect = new C_Rectangle();
	widgtrect->x = bX;
	widgtrect->y = bY;
	widgtrect->h = ht;
	widgtrect->w = wh;

	switch (type)//100 x 32 
	{
	case PLAYBUTTON:ID = "PlayButton";
		fileText = "Assets/playpressed.png";
		mytexture = ResourceManager::getInstance()->getGraphic(fileText);
		break;
	case OPTIONSBUTTON:ID = "OptionsButton";
		//mytexture = ResourceManager::getInstance()->getGraphic("Assets/");
		break;
	case RETURNBUTTON:ID = "ReturnButton";
		fileText = "Assets/Menu.png";
		mytexture = ResourceManager::getInstance()->getGraphic(fileText);
		break;
	case TEXT:ID = "Text";
		fileText = "Assets/font_blue_medium.png";
		mytexture = ResourceManager::getInstance()->getGraphic(fileText);
		break;
	default:
		break;
	}
}

float Widgets::getBeginX()
{
	return beginX;
}

float Widgets::getBeginY()
{
	return beginY;
}

void Widgets::render()
{
	sVideoManager->renderGraphic(fileText, widgtrect, beginX, beginY);
}
