#include "Wall.h"
#include "singletons.h"


Wall::Wall(int _posX, int _posY, int _width, int _height, float speed) : Entity(_posX, _posY, _width, _height)
{
	this->speed = speed;
	vHeight = _height;
	vWidth = _width;
	type = E_Wall;
}

void Wall::update()
{
	updateCollider();
	posX -= speed;
	if (posX < 0-fontRectangle->w)
	{
		posX = (SCREEN_WIDTH*2 - 100) + (rand() % (int)((SCREEN_WIDTH*2 + 100) - (SCREEN_WIDTH*2 - 100) + 1));;
		vHeight = 70 + (rand() % (int)(120 - 70 + 1));
		vWidth = 65 + (rand() % (int)(80 - 65 + 1));
	}
	posY = SCREEN_HEIGHT - vHeight;
}

void Wall::render()
{
	sVideoManager->renderScaledGraphic("Assets/wall.png", fontRectangle, posX - World::getInstance()->playerCamera->cameraRect->x, posY - World::getInstance()->playerCamera->cameraRect->x, vWidth , vHeight );
}

void Wall::updateCollider()
{
	rectCollision.x = posX;
	rectCollision.y = posY;
	rectCollision.h = vHeight;
	rectCollision.w = vWidth;
}

void Wall::setSpeed(float _speed)
{
	speed = _speed;
}

float Wall::getSpeed()
{
	return speed;
}
