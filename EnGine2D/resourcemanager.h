#pragma once
#include "includes.h"

class ResourceManager
{
private:

	static ResourceManager* pInstance;
	ResourceManager();
	std::map<std::string, SDL_Texture*> mGraphicsMapTexture;		


public:
	~ResourceManager();
	static ResourceManager* getInstance();
	void addGraphic(const char* file);
	SDL_Texture* getGraphic(const char* file);
};
