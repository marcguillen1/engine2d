#pragma once
#include "Scene.h"
#include "World.h"
#include "SDL_FontCache.h"

class SceneLevel :
	public Scene
{
public:
	SceneLevel();
	~SceneLevel();
	FC_Font* pixelfont;
	std::string score;
	void init();
	void onDraw();
	void renderScene();
	void update(double seconds_elapsed);
	void keyboardControl(SDL_Event evento);
	void updateCamera();
	void loadAudios();
private:
	World* myworld;
};

