#include "Button.h"


Button::Button() : Widgets()
{
	isCurrentButton = false;
}

void Button::setIsCurrentButton(bool iscurrent)
{
	isCurrentButton = iscurrent;
}

bool Button::getIsCurrentButton(){
	return isCurrentButton;
}

