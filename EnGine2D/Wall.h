#pragma once
#include "Entity.h"
#include "includes.h"
class Wall :
	public Entity
{
private:
	float speed;
	float vHeight, vWidth;
public:
	Wall(int _posX, int _posY, int _width, int _height, float speed);
	void update();
	void render();
	void updateCollider();
	void setSpeed(float _speed);
	float getSpeed();
	~Wall(){};
};

