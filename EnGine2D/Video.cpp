#include "singletons.h"


Video* Video::instance = NULL;

Video::Video()
{
}


Video::~Video()
{
}

void Video::Video_Init()
{
	mFullScreen = false;	// Window mode by default

	//Start SDL 
	SDL_Init(SDL_INIT_EVERYTHING);

	std::string textCaption;
	textCaption = "DAM-ViOD Basic Game Engine SDL2";
	SDL_Surface* icon = IMG_Load("Assets/icon32.png");

	//Create window
	if (mFullScreen) {
		mWindowGame = SDL_CreateWindow(textCaption.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 0, 0, SDL_WINDOW_FULLSCREEN_DESKTOP);
	}
	else {
		mWindowGame = SDL_CreateWindow(textCaption.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1200, 800, SDL_WINDOW_SHOWN);
	}
	if (mWindowGame == NULL)
	{
		std::cout << "Window could not be created! SDL_Error: " << SDL_GetError() << "\n";
		exit(1);
	}
	else
	{
		// Dejo en le codigo todas las opciones posibles de Hints por si hacen falta m�s adelante
		SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "nearest"); // nearest - linear - best
		SDL_SetHint(SDL_HINT_RENDER_DRIVER, "opengl"); // direct3d - opengl - opengles2 - opengles - software
		SDL_SetHint(SDL_HINT_RENDER_VSYNC, "enable vsync"); // disable vsync - enable vsync
		//SDL_SetHint (SDL_HINT_RENDER_OPENGL_SHADERS,"disable shaders"); // disable shaders - enable shaders
		//SDL_SetHint (SDL_HINT_ORIENTATIONS,"LandscapeLeft"); // LandscapeLeft - LandscapeRight - Portrait - PortraitUpsideDown
		//SDL_SetHint (SDL_HINT_IDLE_TIMER_DISABLED,"enable idle timer"); // enable idle timer - disable idle timer
		//SDL_SetHint (SDL_HINT_FRAMEBUFFER_ACCELERATION,"disable 3D acceleration"); // disable 3D acceleration - enable 3D acceleration - enable 3D acceleration, using ...
		//		      X where X is one of the valid rendering drivers. (e.g. "direct3d", "opengl", etc.)

		SDL_SetWindowIcon(mWindowGame, icon);
		mRenderer = SDL_CreateRenderer(mWindowGame, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE | SDL_RENDERER_PRESENTVSYNC);
		SDL_RenderSetLogicalSize(mRenderer, SCREEN_WIDTH, SCREEN_HEIGHT);

		//Get window surface
		Uint32 rmask, gmask, bmask, amask;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
		rmask = 0xff000000;
		gmask = 0x00ff0000;
		bmask = 0x0000ff00;
		amask = 0x000000ff;
#else
		bmask = 0x000000ff;	//??!?!? girado B por R ???
		gmask = 0x0000ff00;
		rmask = 0x00ff0000; //??!?!?
		amask = 0xff000000;
#endif
		mScreen = SDL_CreateRGBSurface(0, SCREEN_WIDTH, SCREEN_HEIGHT, 32, rmask, gmask, bmask, amask);
		mSdlTexture = SDL_CreateTexture(mRenderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, SCREEN_WIDTH, SCREEN_HEIGHT);
		SDL_GetWindowSize(mWindowGame, &mWindowXResolution, &mWindowYResolution);
	}

	SDL_ShowCursor(true);
}

void Video::clearScreen(Uint32 color_key) {
	// ColorKey en ARGB format A se ignora of course
	Uint8 R, G, B, A;
	A = (color_key >> 24) & 255;
	R = (color_key >> 16) & 255;
	G = (color_key >> 8) & 255;
	B = color_key & 255;
	SDL_SetRenderDrawColor(mRenderer, R, G, B, A);		// Color de fondo al limpiar.
	SDL_RenderClear(mRenderer);
}

void Video::updateScreen() {

	SDL_RenderPresent(mRenderer);
	clearScreen(0xFF000000);
}

void Video::renderGraphic(const char* img, C_Rectangle* rect, Sint16 posX, Sint16 posY) {

	SDL_Rect r, rectAux;

	r.x = posX;
	r.y = posY;
	r.w = rect->w;
	r.h = rect->h;

	rectAux.h = rect->h;
	rectAux.w = rect->w;
	rectAux.x = rect->x;
	rectAux.y = rect->y;

	SDL_Texture *origin = sResManager->getGraphic(img);
	SDL_RenderCopy(mRenderer, origin, &rectAux, &r);
}

void Video::renderGraphic_alpha(const char* img, C_Rectangle* rect, Sint16 posX, Sint16 posY, Uint8 alpha) {

	SDL_Rect r, rectAux;

	r.x = posX;
	r.y = posY;
	r.w = rect->w;
	r.h = rect->h;

	rectAux.h = rect->h;
	rectAux.w = rect->w;
	rectAux.x = rect->x;
	rectAux.y = rect->y;

	SDL_Texture *origin = sResManager->getGraphic(img);
	SDL_SetTextureAlphaMod(origin, alpha);
	SDL_RenderCopy(mRenderer, origin, &rectAux, &r);
}

void Video::renderScaledGraphic(const char* img, C_Rectangle* rect, Sint16 posX, Sint16 posY, Uint16 w, Uint16 h) {

	SDL_Rect r, rectAux;

	r.x = posX;
	r.y = posY;
	r.w = w;
	r.h = h;

	rectAux.h = rect->h;
	rectAux.w = rect->w;
	rectAux.x = rect->x;
	rectAux.y = rect->y;
	//rectAux.x = rect->x + (w - rect->x);
	//rectAux.y = rect->y + (h - rect->y);

	SDL_Texture *origin = sResManager->getGraphic(img);
	SDL_RenderCopy(mRenderer, origin, &rectAux, &r);
}
void Video::close()
{
	//Deallocate surface
	SDL_FreeSurface(mScreen);
	mScreen = NULL;

	//Destroy window
	SDL_DestroyWindow(mWindowGame);
	mWindowGame = NULL;

	//Quit SDL subsystems
	SDL_Quit();
}
Video* Video::getInstance()
{
	if (instance == NULL)
	{
		instance = new Video();
	}
	return instance;
}

SDL_Renderer* Video::getRenderer()
{
	return mRenderer;
}

