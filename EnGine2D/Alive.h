#pragma once
#include "Entity.h"
#include "includes.h"
class Alive :
	public Entity
{
private:
protected:
	int hp;
	int tmpX = 0;
	int animationRate = 10;
	int WALKING_ANIMATION_FRAMES = 6;
	std::vector<C_Rectangle*>gSpriteClips;
	const char* spritePath;
public:
	Alive(int _posX, int _posY, int _width, int _height, int _hp);
	Alive(int _posX, int _posY, int _hp);
	~Alive();
};

