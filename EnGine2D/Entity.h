#pragma once
#include "includes.h"
class Entity
{
private:
	bool currentState[2];
	const char* imgPath;
	Entity* baseEntity;
	std::vector<Entity*>vEchild;
	int idEn;

public:
	~Entity();
	int getFlags();
	virtual void render();
	virtual void update();
	void setPosXY(int _posX, int _posY);
	Entity(int _posX, int _posY, int _width, int _height);
	enum typeEntity{ EntityP, E_Alive, E_Character, E_Enemy, E_Walker, E_Flyer, E_Wall, E_Powerup, E_Floor };
	typeEntity type;
	int getPosXY();
	int getPosY();
	int getPosX();
	void setPosX(int _posX);
	void setPosY(int _posY );
	void addChild(Entity* child);
	void removeChild(Entity* child);
	void removeallChild();
	std::string getName();
	void setName(std::string name);
	void setRectCollision(C_Rectangle _rectCollision);
	bool isCollision(C_Rectangle RectA, C_Rectangle RectB);
	C_Rectangle getRectCollision();
	virtual std::string getClassName(){ return "ENTITY"; };
	typeEntity getTypeEntity();
	void SetEntityId(int id);
	int getEntityId();
	void setFlagRT();
	void setFlagLF();
	void initFlags();
protected:
	int posX, posY, width, height, numEn;
	C_Rectangle rectCollision;
	C_Rectangle* fontRectangle;
	Entity(int _posX, int _posY);
	void init(int _posX, int _posY, int _width, int _height);
	enum flags { NOTGOING, GOINGRT, GOINGLF };
	int going;
};

