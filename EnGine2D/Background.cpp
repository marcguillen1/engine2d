#include "Background.h"
#include "singletons.h"



Background::Background(float _bgX, float _bgY, float _maxX, int _width, int _height, const char* imagePath, float speed, std::string name)
{
	initBg(_bgX, _bgY,_maxX, _width, _height, imagePath, speed, name);
}


Background::~Background()
{
}

void Background::initBg(float _bgX, float _bgY, float _maxX, int _width, int _height, const char* imagePath, float speed, std::string name)
{
	bgX = _bgX;
	bgY = _bgY;
	maxX = _maxX;
	width = _width;
	height = _height;
	fontRect = new C_Rectangle();
	fontRect->x = bgX;
	fontRect->y = 0;
	fontRect->w = width;
	fontRect->h = height;
	this->imagePath = imagePath;
	this->speed = speed;
	this->name = name;
}

void Background::update()
{
	bgX -= speed;
	if (bgX <= -maxX)
	{
		bgX = maxX;
	}
}

void Background::render()
{
	sVideoManager->renderScaledGraphic(imagePath, fontRect, bgX /*- World::getInstance()->playerCamera->cameraRect->x*/, bgY /*- World::getInstance()->playerCamera->cameraRect->y*/, 1000, 500);
}

void Background::setSpeed(float _speed)
{
	speed = _speed;
}

float Background::getSpeed()
{
	return speed;
}

std::string Background::getName()
{
	return name;
}
