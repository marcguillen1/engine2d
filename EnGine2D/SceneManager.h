#pragma once
#include "includes.h"
#include "SceneLevel.h"
#include "SceneMenu.h"
#include "SceneGameOver.h"

class SceneManager
{

private:
	static SceneManager* instance;
	SceneManager();
	Scene* currentScene;

public:
	~SceneManager();
	bool sceneIscharged;
	static SceneManager* getInstance();
	void loadScene(int _sceneID);
	void updateScene();
	void renderScene();
	void getEvents(SDL_Event evento);
	void clearScene();
	void startScene();
};

