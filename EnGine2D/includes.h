#ifndef INCLUDES_H
#define INCLUDES_H

// Platform System Compile
#define __PC_SDL2__


// N�mero m�ximo de jugadores del juego
#define MAX_PLAYERS 1

// Definir para permitir realizar una misma acci�n con varios botones
#define ALLOW_MULTIPLE_BUTTONS_PER_ACTION


///////////////////////////////////////////////////////////////////////////////////
///////////////////////// L I B R A R I E S ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////


#ifdef WIN32
#include "SDL_image.h"
#include "SDL_mixer.h"
#endif


// C++ Libraries
#include <string>
#include <vector>
#include <map>
#include <list>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <algorithm>
#include "math.h"
#include "SDL.h"
#include "SDL_image.h"



///////////////////////////////////////////////////////////////////////////////////
////////////  S Y S T E M   F U N C T I O N S   A N D   S T R U C T S  ////////////
///////////////////////////////////////////////////////////////////////////////////

// Esta estructura es una replica a SDL_Rect
// Debe tener el mismo formato para igualarse
// x e y con signo. Un grafico puede situarse en negativo para aparecer por el lado Izq.

//! Struct C_Rectangle.
/*! Replica of SDL_Rect. */
typedef struct { 
	  Sint16 x;
	  Sint16 y;
	  Uint16 w;
	  Uint16 h;
} C_Rectangle;

//! Struct Point.
/*! A point in 2D. */
struct Point { 
	Sint32 x;
	Sint32 y;

	bool operator==(const Point& a) const
	{
		return (x == a.x && y == a.y);
	}
	bool operator!=(const Point& a) const
	{
		return (x != a.x || y != a.y);
	}
};

//! Struct Triangle.
/*! Struct that consists of 3 points. */
typedef struct {
	Point a;
	Point b;
	Point c;
} C_Triangle;

// Tiempo pasado entre frames
extern Uint32 global_elapsed_time;
// Tiempo inicial
extern Uint32 start_time;

enum typeOfSound { MENU, PLAY, JUMP, GAMEOVER, EXP, COLLISION };

//Elegancia personificada para eliminar cooosas que son vectores de punteros
template <class C> void FreeClear( C & cntr ) {
    for ( typename C::iterator it = cntr.begin(); 
              it != cntr.end(); ++it ) {
    	delete * it;
    }
    cntr.clear();
}

//Defines de l�mites para enteros
#define UINT_MINVALUE 0
#define UINT8_MAXVALUE 255
#define SINT8_MAXVALUE 127
#define SINT8_MINVALUE -128
#define UINT16_MAXVALUE 65535
#define SINT16_MAXVALUE 32767
#define SINT16_MINVALUE -32768
#define UINT32_MAXVALUE 4294967295
#define SINT32_MAXVALUE 2147483647
#define SINT32_MINVALUE -2147483648

///////////////////////////////////////////////////////////////////////////////////
///////////////////////// G A M E   D E F I N I T I O N S /////////////////////////
///////////////////////////////////////////////////////////////////////////////////

#define INACTIVITY_TIME 30 //Time to auto-pause the game (in seconds). ONLY WORKS IN DEMOTGS

#define SCREEN_WIDTH 854	// PC- 854, 1280, 1920
#define SCREEN_HEIGHT 480	// PC- 480, 720, 1080

#define BPP 32

///////////////////////////////////////////////////////////////////////////////////
//////////////  G A M E   F U N C T I O N S   A N D   S T R U C T S  //////////////
///////////////////////////////////////////////////////////////////////////////////

#endif
