#ifndef AUDIO_H
#define AUDIO_H
#include "includes.h"
#include "bass.h"

class Audio
{
	public:
		static int numEntities;
		typeOfSound type;

		Audio(typeOfSound _type);
		~Audio(){};
		
		void init();
		void loadSound();
		void playSound();
		void stopSound();
		void setVolume(float volume);
		void sampleRate(float freq);
		void setSpeed(float speed);

		HSAMPLE hSample;
		HCHANNEL hSampleChannel;
	
};

#endif 