#pragma once
#include "includes.h"
class Scene
{
public:
	SDL_Event mEvento;
	Scene();
	~Scene();

	virtual void init();
	virtual void onDraw();
	virtual void renderScene();
	virtual void update(double seconds_elapsed);
    virtual void keyboardControl(SDL_Event kevent);
	virtual void joystickControl();
	virtual void onKeyPressed(SDL_KeyboardEvent event);
	virtual void loadAudios();

	virtual std::string getClassName() { return "Stage"; };
	int id_font_small, id_font_medium, id_font_large, id_font_xxl;

	
};

