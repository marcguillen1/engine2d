#include "audio.h"

Audio::Audio(typeOfSound _type)
{
	type = _type;
	hSample = 0;
	hSampleChannel = 0;
	init();
}

void Audio::init()
{
	BASS_Init(1, 44100, 0, 0, NULL);

}

void Audio::loadSound()
{

	switch(type)
	{
		case MENU:
			hSample = BASS_SampleLoad(false, "Assets/mainmenu.mp3",0,0,3,BASS_SAMPLE_LOOP);
			break;
		case PLAY:
			hSample = BASS_SampleLoad(false, "Assets/ingame.mp3", 0, 0, 3, BASS_SAMPLE_LOOP);
			break;
		case GAMEOVER:
			hSample = BASS_SampleLoad(false, "Assets/gameover.mp3", 0, 0, 3, BASS_SAMPLE_LOOP);
			break;
		case JUMP:
			hSample = BASS_SampleLoad(false, "Assets/jump.mp3", 0, 0, 3, BASS_SAMPLE_FX);
			break;
		case COLLISION:
			hSample = BASS_SampleLoad(false, "Assets/hit.mp3", 0, 0, 3, BASS_SAMPLE_FX);
			break;
		case EXP:
			hSample = BASS_SampleLoad(false, "Assets/exp.mp3", 0, 0, 3, BASS_SAMPLE_FX);
			break;

	}
}

void Audio::playSound()
{
	hSampleChannel  = BASS_SampleGetChannel(hSample,false);
	BASS_ChannelPlay(hSampleChannel , true);
}

void Audio::stopSound()
{
	BASS_ChannelStop(hSampleChannel);
}
void Audio::setVolume(float volume)
{
	BASS_ChannelSetAttribute(hSampleChannel,BASS_ATTRIB_VOL,volume);
}

void Audio::sampleRate(float freq)
{
	BASS_ChannelSetAttribute(hSampleChannel,BASS_ATTRIB_FREQ, freq);
}

void Audio::setSpeed(float speed)
{
	BASS_ChannelSetAttribute(hSampleChannel,BASS_ATTRIB_MUSIC_SPEED,speed);
}