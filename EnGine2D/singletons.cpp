#include "singletons.h"

Video* sVideoManager;
ResourceManager* sResManager;
SceneManager* sSceneManager;
//SceneDirector*		sDirector;
//Control*			sInputControl;
AudioManager*		sAudioManager;
//FxGfx*				sFxGfx;	
//TextManager*		sTxtManager;

void instanceSingletons(){
sVideoManager = Video::getInstance();
sResManager = ResourceManager::getInstance();
sSceneManager = SceneManager::getInstance();
	//sDirector	  = SceneDirector::getInstance();
	//sInputControl = Control::getInstance();
sAudioManager = AudioManager::getInstance();
	//sFxGfx		  = FxGfx::getInstance();
    //sTxtManager	  = TextManager::getInstance();
}
