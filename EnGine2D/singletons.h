#ifndef SINGLETONS_H
#define SINGLETONS_H
#include "includes.h"
#include "Video.h"
#include "resourcemanager.h"
#include "SceneManager.h"
#include "AudioManager.h"
//#include "SoundManager.h"
//#include "Control.h"
//#include "FxGfx.h"
//#include "TextManager.h"

extern Video* sVideoManager;	/*!<  Handler for rendering*/
extern AudioManager*	sAudioManager;	/*!<  Handler for storing and playing audio*/
extern ResourceManager*	sResManager;/*!<  Handler for loading and unloading graphical assets*/
extern SceneManager* sSceneManager;/*!<  Handler for Scenes*/	
//extern Control*			sInputControl;	/*!<  Handler for user interface*/
//extern FxGfx*			sFxGfx;			/*!<  Special effects on surfaces (painting, resizing, etc.)*/
//extern TextManager*		sTxtManager;	/*!<  Loads all texts and fonts of the game*/

void instanceSingletons();

#endif
