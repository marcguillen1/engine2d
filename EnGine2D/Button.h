#pragma once
#include "Widgets.h"
class Button : public Widgets
{
public:
	Button();
	~Button();
	void setIsCurrentButton(bool iscurrent);
	bool getIsCurrentButton();
private:
	bool isCurrentButton;
};

