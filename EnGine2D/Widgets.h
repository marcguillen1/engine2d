#pragma once
#include "includes.h"

enum Type { PLAYBUTTON, OPTIONSBUTTON, RETURNBUTTON, TEXT };

class Widgets
{
public:

	Widgets();
	~Widgets();

	C_Rectangle* widgtrect;
	const char* fileText;

	void initWidget(float posX, float posY, float beginX, float beginY, float width, float height, Type type);
	void setName(std::string name);

	void update();
	void render();

	void changeID(std::string newName);

	float getBeginX();
	float getBeginY();
	float getWidth();
	float getHeight();

	std::string ID;
	int numID;

	SDL_Texture* mytexture;

	virtual std::string getClassName(){ return "Widget"; };

protected:
	float posX;
	float posY;
	float beginX;
	float beginY;
	float width;
	float height;

};

