#include "singletons.h"


SceneManager* SceneManager::instance = NULL;
SceneManager::SceneManager()
{
	currentScene = NULL;
}

SceneManager::~SceneManager()
{

}

SceneManager* SceneManager::getInstance()
{
	if (instance == NULL)
	{
		instance = new SceneManager();
	}
	return instance;
}
void SceneManager::loadScene(int _sceneID)
{
	clearScene();

	switch (_sceneID)
	{
	case 0:
		currentScene = new SceneMenu();
		break;
	case 1:
		currentScene = new SceneLevel();
		break;
	case 2:
		currentScene = new SceneGameOver();
		break;
	default:
		break;
	}
	sceneIscharged = false;
}

void SceneManager::clearScene()
{
	if (currentScene != NULL)
	{
		currentScene = NULL;
		delete currentScene;
	}
}

void SceneManager::startScene() {
	// Initialize and run the scene
	if (!sceneIscharged)
	{
		currentScene->init();
		sceneIscharged = true;
	}
}

void SceneManager::updateScene()
{
	currentScene->update(global_elapsed_time);
}

void SceneManager::renderScene()
{
	currentScene->renderScene();
}

void SceneManager::getEvents(SDL_Event kevento)
{

	currentScene->keyboardControl(kevento);
}