#pragma once
#include "Alive.h"
#include "includes.h"

class Character :
	public Alive
{
private:
	enum state{isgrounded, isonair};
	state currentState;
	float gravity, jumpForce, currentJumpForce;
	bool jumping, hasjump;
public:
	Character(int _posX, int _posY, int _width, int _height, int _hp);
	Character(int _posX, int _posY, int _hp);
	Character();
	~Character();
	void CreateAnimationWalk();
	void update();

	void updateCollider();
	void render();
	void Animate(int _characterX, int characterY);
	void jump();
	bool checkCollisions(std::vector<C_Rectangle> vCol);
	void setJumping(bool isjumping);
};

