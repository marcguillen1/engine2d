﻿#include "Timer.h"
#include "singletons.h"
//#include "Entity.h"
//#include "FileManager.h"


Uint32 global_elapsed_time;
Uint32 start_time;
SDL_Event mEvento;

//#define FORCE_FIXED_TIME

//---------------------------------------------------------------------------

// -------------------------------------------------------------------------------- VIDEO --------------------------------------------------------------------------------


// -------------------------------------------------------------------------------- FIN VIDEO --------------------------------------------------------------------------------

//---------------------------------------------------------------------------------  MAIN ---------------------------------------------------------------------------------
void mainLoop() {
//TIMES//
	global_elapsed_time = 0;
	Timer* globalTimer = new Timer();
	Timer* capTimer = new Timer();
	Uint32 last_time = 0;
////////

//Scenes//
	
///////

	int frame = 0;
	int countedFrames = 0;

	globalTimer->start();
	start_time = globalTimer->getTicks();
	while (true) {
		sSceneManager->startScene();
		// Start cap timer
		capTimer->start();
		//Calculate and correct fps
		float avgFPS = countedFrames / (globalTimer->getTicks() / 1000.f);
		if (avgFPS > 2000000)
		{
			avgFPS = 0;
		}

		//Update events
		//sInputControl->update();

		//Update logic
		global_elapsed_time = globalTimer->getTicks();
		//last_time =  globalTimer->getTicks();

		//sDirector->getCurrentScene()->onUpdate();
	
			if (sSceneManager->sceneIscharged)sSceneManager->updateScene();
			if (sSceneManager->sceneIscharged)sSceneManager->renderScene();
		
		
		//if(sDirector->getCurrentScene()->isLoaded()){
		//	sDirector->getCurrentScene()->onDraw();
		//}
		sVideoManager->updateScreen();

			while (SDL_PollEvent(&mEvento)) {
			if (mEvento.type == SDL_QUIT) {//Aquí llamaremos a una funcion de scenemanager que actualice inputs 			
				exit(0);
			}
			else sSceneManager->getEvents(mEvento);
		}
	}
}

int main(int argc, char* argv[]) {
	instanceSingletons();
	//sTxtManager->init();	//Se carga primero el sistema de Text, que se usa en Scenes
	//sDirector->init();		//Inicializamos el Director
	//Init basic graphics
	//----Cosas----
	//Init basic sounds and music
	//----Cosas----
	//sSoundManager->addSound("Assets/snd/jingle.ogg", FX);
	//sInputControl->showCursor(true);
	sVideoManager->Video_Init();
	sSceneManager->loadScene(0);

	mainLoop();

	return 0;
}
//--------------------------------------------------------------------------------- FIN MAIN ---------------------------------------------------------------------