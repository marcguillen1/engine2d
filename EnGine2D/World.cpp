#include "World.h"
#include "singletons.h"


ResourceManager* rmanager = ResourceManager::getInstance();

Camera* World::playerCamera = NULL;
World* World::instance = NULL;

World::World()
{
	initWorld();
}

World::~World()
{
	removeall();
	playerCamera = NULL;
	instance = NULL;
}

World* World::getInstance()
{
	if (!instance) {
		instance = new World();
	}
	return instance;
}

void World::initWorld()
{
	//TIMES//
	////////
	tickTime = global_elapsed_time;

	//BACKGROUND//
	initBackgrounds();
	//////////


	//CAMERA//
	C_Rectangle* mainCameraRect = new C_Rectangle();
	mainCameraRect->x = 0;
	mainCameraRect->y = 0;
	mainCameraRect->w = SCREEN_WIDTH;
	mainCameraRect->h = SCREEN_HEIGHT;
	playerCamera = new Camera(mainCameraRect);
	//////////


	//CHARACTER//
	mainCharacter = new Character(100, 115, 87 , 100, 100);
	mainCharacter->CreateAnimationWalk();
	vEntities.push_back(mainCharacter);
	/////////////

	//WALLS//
	Wall* wall_01 = new Wall(1000, SCREEN_HEIGHT -120, 72, 118, 5);
	Wall* wall_02 = new Wall(2000, SCREEN_HEIGHT - 120, 72, 118, 5);
	wall_01->SetEntityId(2);
	wall_01->SetEntityId(3);
	vEntities.push_back(wall_01);
	vEntities.push_back(wall_02);

	/////////

	//GROUND//
	ground = C_Rectangle();
	ground.x = 0;
	ground.y = 440;
	ground.h = 10;
	ground.w = SCREEN_WIDTH;

	score = 0;
	oldscore = 0;
}

void World::setPlayerEntity(Character* entity)
{
	mainCharacter = entity;
}

Character* World::getPlayerEntity()
{
	return mainCharacter;
}

C_Rectangle World::getGround()
{
	return ground;
}


void World::setMainCamera()
{

}
void World::initBackgrounds()
{
	Background* bgGround1 = new Background(0, SCREEN_HEIGHT, 1000, 1000, 83, "Assets/ground.png", 5, "Ground");
	Background* bgGround2 = new Background(1000, SCREEN_HEIGHT, 1000, 1000, 83, "Assets/ground.png", 5, "Ground");
	Background* bgLowCity1 = new Background(0, 0, 1000, 1200, 1000, "Assets/CityBig.png", 3, "lowcity");
	Background* bgLowCity2 = new Background(1000, 0, 1000, 1200, 1000, "Assets/CityBig.png", 3, "lowcity");
	Background* bgMidCity1 = new Background(0, 0, 1000, 1200, 1000, "Assets/City2.png", 2, "midcity");
	Background* bgMidCity2 = new Background(1000, 0, 1000, 1200, 1000, "Assets/City2.png", 2, "midcity");
	Background* sky1 = new Background(0, 0, 1000, 1200, 1000, "Assets/SkyEngine.png", 1, "sky");
	Background* sky2 = new Background(1000, 0, 1000, 1200, 1000, "Assets/SkyEngine.png", 1, "sky");

	backgrounds.push_back(sky1);
	backgrounds.push_back(sky2);
	backgrounds.push_back(bgMidCity1);
	backgrounds.push_back(bgMidCity2);
	backgrounds.push_back(bgLowCity1);
	backgrounds.push_back(bgLowCity2);
	backgrounds.push_back(bgGround1);
	backgrounds.push_back(bgGround2);


}
void World::renderWorld()
{
	renderBackgrounds();
	for each(Entity* e in vEntities)
	{
		e->render();
	}
}

void World::updateWorld(float time)
{
	updateBackgrounds();
	for each(Entity* e in vEntities)
	{
		e->update();
	}
	if (oldscore < score - 500)
	{
		sAudioManager->getAudio(EXP)->loadSound();
		sAudioManager->getAudio(EXP)->playSound();
		oldscore = score;
		for each (Wall* w in getWalls())
		{
			w->setSpeed(w->getSpeed() + 1);
		}
		for each (Background* b in backgrounds)
		{
			if (b->getName() != "sky")b->setSpeed(b->getSpeed() + 1);
		}
	}
	score++;
}

void World::updateBackgrounds()
{
	for each (Background* b in backgrounds)
	{
		b->update();
	}
}

void World::renderBackgrounds()
{
	for each (Background* b in backgrounds)
	{
		b->render();
	}
}

Entity* World::getEntity(int id)
{
	for each (Entity* e in vEntities)
	{
		if (e->getEntityId() == id)
		{
			return e;
		}
	}
}

std::vector<C_Rectangle> World::getEntitiesColByType(Entity::typeEntity type){

	std::vector<C_Rectangle> vCol;
	for each (Entity* e in vEntities)
	{
		if (e->getTypeEntity() == type)
		{
			C_Rectangle currentEntityRect = e->getRectCollision();
			vCol.push_back(currentEntityRect);
		}
	}
	return vCol;
}

std::vector<Wall*> World::getWalls()
{
	std::vector<Wall*> vWall;
	for each (Wall* e in vEntities)
	{
	  vWall.push_back(e);
	}
	return vWall;
}

void World::removeallEntities()
{
	for each (Entity* e in vEntities)
	{
		delete e;
	}
}

void World::removeall()
{
	removeallEntities();
	removeBackgrounds();
	playerCamera->~Camera();
}

void World::removeBackgrounds()
{
	for each (Background* b in backgrounds)
	{
		delete b;
	}
}