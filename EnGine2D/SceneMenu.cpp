#include "SceneMenu.h"
#include "singletons.h"


SceneMenu::SceneMenu() : Scene()
{
	play = false;
	playButton = new Button();
	homeButton = new Button();
	widgets.push_back(playButton);
	widgets.push_back(homeButton);
	playButton->setIsCurrentButton(true);
	bg = new C_Rectangle();
}


SceneMenu::~SceneMenu()
{
}

void SceneMenu::init(){
	playButton->initWidget((SCREEN_WIDTH/2)-64, (SCREEN_HEIGHT/2), 0, 0, 132, 32, PLAYBUTTON);
	homeButton->initWidget(playButton->getBeginX(), playButton->getBeginY() + 48, 0, 0, 132, 32, RETURNBUTTON);
	sAudioManager->getAudio(MENU)->loadSound();
	sAudioManager->getAudio(MENU)->playSound();
	sAudioManager->getAudio(MENU)->setVolume(0.1);
	bg->x = 0;
	bg->y = 0;
	bg->w = 2400;
	bg->h = 1000;
}

void SceneMenu::update(double seconds_elapsed)
{
	if (play)
	{
		sSceneManager->loadScene(1);
		sAudioManager->getAudio(MENU)->stopSound();
	}
}

void SceneMenu::keyboardControl(SDL_Event kevent)
{

	mEvento = kevent;
	SDL_PumpEvents();

	if (mEvento.type == SDL_KEYDOWN) {
		switch (mEvento.key.keysym.scancode) {
		case SDL_SCANCODE_UP:
			if (!playButton->getIsCurrentButton()){
				playButton->setIsCurrentButton(true);
				playButton->fileText = "Assets/playpressed.png";
				homeButton->setIsCurrentButton(false);
				homeButton->fileText = "Assets/Menu.png";
			}
			break;
		case SDL_SCANCODE_DOWN: 
			if (!homeButton->getIsCurrentButton()){
				homeButton->setIsCurrentButton(true);
				homeButton->fileText = "Assets/Menupressed.png";
				playButton->setIsCurrentButton(false);
				playButton->fileText = "Assets/play.png";
			}
			break;
		case SDL_SCANCODE_LEFT:
			break;
		case SDL_SCANCODE_RIGHT:
			break;
		case SDL_SCANCODE_RETURN: if (playButton->getIsCurrentButton())play = true;
			break;
		default:
			break;
		}
	}
}

void SceneMenu::renderScene()
{
	sVideoManager->renderScaledGraphic("Assets/mainmenuscreen.png", bg, 0, 0, 855, 480);
	for each (Widgets* w in widgets)
	{
		w->render();
	}

}