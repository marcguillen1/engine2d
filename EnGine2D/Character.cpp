#include "Character.h"
#include "World.h"
#include "singletons.h"

Character::Character(int _posX, int _posY, int _width, int _height, int _hp) :Alive(_posX, _posY, _width, _height, _hp)
{
	hp = _hp;
	initFlags();
	spritePath = "Assets/character.png";
	gravity = 9.8;
	jumpForce = 18;
	type = E_Character;
}

Character::~Character()
{
}


void Character::CreateAnimationWalk()
{
	for (int i = 0; i < Alive::WALKING_ANIMATION_FRAMES; i++)
	{
		C_Rectangle* rectWalk = new C_Rectangle();
		rectWalk->h = 128;
		rectWalk->w = 87;
		rectWalk->x = tmpX;
		rectWalk->y = 0;
		tmpX = tmpX + 87;
		gSpriteClips.push_back(rectWalk);
	}
}

void Character::update()
{
	updateCollider();
	if (!isCollision(World::getInstance()->getPlayerEntity()->getRectCollision(), World::getInstance()->getGround())){
		currentState = isonair;
		posY += gravity;
	}
	else {
		currentState = isgrounded;
		hasjump = true;
	}

	if (checkCollisions(World::getInstance()->getEntitiesColByType(E_Wall)))
	{
		sAudioManager->getAudio(COLLISION)->loadSound();
		sAudioManager->getAudio(COLLISION)->playSound();
		sAudioManager->getAudio(PLAY)->stopSound();
		sSceneManager->loadScene(2);
	}


	if (jumping)
	{
		if (hasjump) {
			currentJumpForce = jumpForce;
			hasjump = false;
			sAudioManager->getAudio(JUMP)->loadSound();
			sAudioManager->getAudio(JUMP)->playSound();
			/*sAudioManager->getAudio(PLAY)->setVolume(10);*/
			sAudioManager->getAudio(JUMP)->setVolume(1);
		}
		if (!hasjump && currentJumpForce > 0)
		{
			currentJumpForce-=0.3;
		}
		else {
			jumping = false;
		}
		posY -= currentJumpForce;
	}else
	{
		if (going == GOINGLF)
		{
			if (posX >= 10)
			{
				posX -= 10;
			}
		}
		else if (posX <= SCREEN_WIDTH - 100 && going == GOINGRT)
		{
			posX += 10;
		}
	}
}

void Character::render()
{
	Animate(posX, posY);
}

void Character::Animate(int _characterX, int _characterY)
{
	int frameToDraw = ((global_elapsed_time - start_time)*animationRate / 1000) % WALKING_ANIMATION_FRAMES;
	C_Rectangle* currentClip = gSpriteClips[frameToDraw];
	if (jumping)
	{
		currentClip = gSpriteClips[3];
	}
	sVideoManager->renderGraphic(spritePath, currentClip, posX - World::playerCamera->cameraRect->x, posY - World::playerCamera->cameraRect->x);
}

void Character::updateCollider()
{
	rectCollision.x = posX;
	rectCollision.y = posY;
}

void Character::setJumping(bool isjumping)
{
	jumping = isjumping;
}

bool Character::checkCollisions(std::vector<C_Rectangle> vCol)
{
	for each (C_Rectangle c in vCol)
	{
		if (isCollision(rectCollision, c))
		{
			return true;
		}
	}
	return false;
}

