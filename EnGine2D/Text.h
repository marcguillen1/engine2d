#pragma once
#include "Widgets.h"
class Text : public Widgets
{
private:
	C_Rectangle* currentRect;
public:
	Text();
	~Text();
	void drawText(std::string text, int x, int y);
};

