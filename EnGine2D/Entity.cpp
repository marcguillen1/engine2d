#include "Entity.h"
#include "singletons.h"

Entity::Entity(int _posX, int _posY, int _width, int _height)
{
	init(_posX, _posY, _width, _height);
	initFlags();
}

Entity::Entity(int _posX, int _posY)
{
	posX = _posX;
	posY = _posY;
}


Entity::~Entity()
{
	if (baseEntity != NULL)
	{
		baseEntity->removeChild(this);
	}
	removeallChild();
	vEchild.clear();
}

void Entity::init(int _posX, int _posY, int _width, int _height)
{
	numEn++;
	idEn = numEn;

	posX = _posX;
	posY = _posY;

	fontRectangle = new C_Rectangle();
	fontRectangle->w = _width;
	fontRectangle->h = _height;
	fontRectangle->x = 0;
	fontRectangle->y = 0;
	rectCollision.y = posY;
	rectCollision.x = posX;
	rectCollision.w = _width;
	rectCollision.h = _height;
}

void Entity::update()
{
	if (!vEchild.empty())
	{
		for (unsigned int i = 0; i < vEchild.size(); i++)
		{
			vEchild.at(i)->update();
		}
	}
}

void Entity::render()
{
	sVideoManager->renderGraphic(imgPath, fontRectangle, posX, posY);
}

void Entity::addChild(Entity* child)
{
	vEchild.push_back(child);
	child->baseEntity = this;
}
void Entity::removeChild(Entity* child)
{
	std::vector<Entity*>::iterator it_vEc;
	it_vEc = std::find(vEchild.begin(), vEchild.end(), child);
	if (*it_vEc == child)
	{
		vEchild.erase(it_vEc);
		child->baseEntity == NULL;
	}
}
void Entity::removeallChild()
{
	std::vector<Entity*>::iterator it_vEc;
	it_vEc = vEchild.begin();
	while (it_vEc != vEchild.end())
	{
		vEchild.erase(it_vEc);
		it_vEc++;
	}
	
}

void Entity::setPosXY(int _posX, int _posY)
{
	posX = _posX;
	posY = _posY;
}
int Entity::getPosXY()
{
	return posX, posY;

}
int Entity::getPosX()
{
	return posX;

}
void Entity::setPosX(int _posX)
{
	posX = _posX;
}
void Entity::setPosY(int _posY)
{
	posY = _posY;
}
int Entity::getPosY()
{
	return posY;

}

void Entity::initFlags()
{
	going = NOTGOING;
}

void Entity::setFlagLF()
{
	going = GOINGLF;
}

void Entity::setFlagRT()
{
	going = GOINGRT;
}

int Entity::getFlags()
{
	return going;
}

void Entity::setRectCollision(C_Rectangle _rectCollision)
{
	rectCollision = _rectCollision;
}

C_Rectangle Entity::getRectCollision()
{
	return rectCollision;
}

bool Entity::isCollision(C_Rectangle RectA, C_Rectangle RectB){
	if ((RectA.x < RectB.x + RectB.w) &&
		(RectB.x < RectA.x + RectA.w) &&
		(RectA.y < RectB.y + RectB.h) &&
		(RectB.y < RectA.y + RectA.h)){
		return true;
	}
	return false;
}

Entity::typeEntity Entity::getTypeEntity(){
	return type;
}

void Entity::SetEntityId(int id)
{
	idEn = id;
}

int Entity::getEntityId()
{
	return idEn;
}


