#include "SceneLevel.h"
#include "singletons.h"



SceneLevel::SceneLevel()
{
	pixelfont = FC_CreateFont();
}

void SceneLevel::init()
{
	myworld = World::getInstance();
	myworld->World::World();
	loadAudios();
	FC_LoadFont(pixelfont, sVideoManager->mRenderer, "Assets/slkscr.ttf", 35, FC_MakeColor(250, 250, 250, 255), TTF_STYLE_BOLD);

}

void SceneLevel::onDraw()
{
	renderScene();
}

void SceneLevel::renderScene()
{

   myworld->renderWorld();
   FC_Draw(pixelfont, sVideoManager->mRenderer, 5, 0, "score: ");
   FC_Draw(pixelfont, sVideoManager->mRenderer, 155, 0, score.c_str());

}

void SceneLevel::update(double seconds_elapsed)
{
	myworld->updateWorld(seconds_elapsed);
	score = std::to_string(myworld->score);
}

void SceneLevel::keyboardControl(SDL_Event kevent)
{

	mEvento = kevent;
	myworld->getPlayerEntity();
	SDL_PumpEvents();

	if (mEvento.type == SDL_KEYDOWN) {
		switch (mEvento.key.keysym.scancode) {
		case SDL_SCANCODE_UP:
			break;
		case SDL_SCANCODE_DOWN:
			break;
		case SDL_SCANCODE_LEFT:myworld->getPlayerEntity()->setFlagLF();
			break;
		case SDL_SCANCODE_RIGHT:myworld->getPlayerEntity()->setFlagRT();
			break;
		case SDL_SCANCODE_SPACE:myworld->getPlayerEntity()->setJumping(true);
			break;
		case SDL_SCANCODE_ESCAPE: myworld->~World(); sSceneManager->loadScene(2);
			break;
		default:
			break;
		}
	}
	
	if (mEvento.type == SDL_KEYUP)
		switch (mEvento.key.keysym.scancode) {
		case SDL_SCANCODE_UP:
			break;
		case SDL_SCANCODE_DOWN:
			break;
		case SDL_SCANCODE_LEFT:if (myworld->getPlayerEntity()->getFlags() == 2) myworld->getPlayerEntity()->initFlags();
			break;
		case SDL_SCANCODE_RIGHT:if (myworld->getPlayerEntity()->getFlags() == 1)myworld->getPlayerEntity()->initFlags();
			break;
		default:
			break;
	}
}

void SceneLevel::updateCamera()
{

}

void SceneLevel::loadAudios()
{
	sAudioManager->addAudio(PLAY);
	sAudioManager->getAudio(PLAY)->loadSound();
	sAudioManager->getAudio(PLAY)->playSound();
	sAudioManager->getAudio(PLAY)->setVolume(0.1);
}


SceneLevel::~SceneLevel()
{
}

