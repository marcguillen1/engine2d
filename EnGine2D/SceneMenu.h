#pragma once
#include "includes.h"
#include "Scene.h"
#include "Button.h"

class SceneMenu : public Scene
{
public:
	SceneMenu();
	~SceneMenu();

	bool play;
	Button* playButton;
    Button* homeButton;
	std::vector<Widgets*> widgets;
	std::vector<Widgets*>::iterator it;
	C_Rectangle* bg;

	void init();
	void renderScene();
	void update(double seconds_elapsed);
	void keyboardControl(SDL_Event evento);
};
