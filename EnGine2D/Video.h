#pragma once
#include "includes.h"
class Video
{
private:
	static Video* instance;
	enum RenderTarget { DEFAULT, MAIN_SCREEN };
	Video();
	SDL_Surface*	mScreen;			/*!< Screen surface target */
	bool			mFullScreen;		/*!< If true all render is fullscreen */
	RenderTarget 	mTarget;			/*!< Where to render*/
	SDL_Window*		mWindowGame;		/*!< Handler for window by O.S. */
	int				mWindowXResolution;	/*!< Width of game window resolution */
	int				mWindowYResolution;	/*!< Height of game window resolution */		/*!< Render context for window */
	SDL_Texture*	mSdlTexture;		/*!< Screen texture to show in window */
public:
	~Video();
	void Video_Init();
	void clearScreen(Uint32 color_key);
	void updateScreen();
	void renderGraphic(const char* img, C_Rectangle* rect, Sint16 posX, Sint16 posY);
	void renderGraphic_alpha(const char* img, C_Rectangle* rect, Sint16 posX, Sint16 posY, Uint8 alpha);
	void renderScaledGraphic(const char* img, C_Rectangle* rect, Sint16 posX, Sint16 posY, Uint16 w, Uint16 h);
	void close();
	SDL_Renderer*	mRenderer;
	static Video* getInstance();
	SDL_Renderer* getRenderer();

};

