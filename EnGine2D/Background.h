#pragma once
#include "includes.h"
class Background
{
private: 
	int bgX, bgY, maxX, width, height;
	C_Rectangle* fontRect;
	float speed;
	std::string name;
	const char* imagePath;
public:
	Background(float _bgX, float _bgY, float _maxX, int _width, int _height, const char* imagePath, float speed, std::string name);
	void initBg(float _bgX, float _bgY, float _maxX, int _width, int _height, const char* imagePath, float speed, std::string name);
	void update();
	void render();
	void setSpeed(float _speed);
	float getSpeed();
	std::string getName();
	~Background();
};

