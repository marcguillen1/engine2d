#include "singletons.h"


AudioManager* AudioManager::instance = NULL;

AudioManager* AudioManager::getInstance()
{
	if (instance == NULL) {
		instance = new AudioManager();
	}
	return instance;
}

AudioManager::AudioManager()
{
}

AudioManager::~AudioManager()
{
	mSoundMapAudio.clear();
}

void AudioManager::addAudio(typeOfSound audiotype)
{
	Audio* audio = new Audio(audiotype);
	mSoundMapAudio.insert(std::pair<typeOfSound, Audio*>(audiotype, audio));
}

Audio* AudioManager::getAudio(typeOfSound audiotype)
{
	std::map<typeOfSound, Audio*>::iterator it;
	it = mSoundMapAudio.find(audiotype);

	if (it == mSoundMapAudio.end())
	{
		std::cout << "Adding Audio" << audiotype << "(first request)";
		addAudio(audiotype);
		it = mSoundMapAudio.find(audiotype);
	}
	return it->second;
}


